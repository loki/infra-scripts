#!/bin/bash

# like nonfatal edo, just echoes, doesn't die. 
# also adds some newlines just so it's easier to see what's going on in the logs
_edo() {
  echo -e "\n# ${@}\n" 1>&2
  "${@}"
}

set -e

chgrp paludisbuild /dev/tty

# In case the repo URL and the repo name differ, e.g:
# URL = gitlab.exherbo.org/user/user-exheres ($REPO here)
# Name = user ($PALUDIS_REPO here)
PALUDIS_REPO=${PALUDIS_REPO:=${REPO}}

# Enable our repo and all repos that have it as a master according to ::unavailable
_edo cave sync
_edo cave resolve -x -Ks repository/${PALUDIS_REPO} $(cave print-ids -m "repository/*[.dependencies<repository/${PALUDIS_REPO}]" -f '%c/%p\n')

rm -Rf /tmp/ciwork
mkdir /tmp/ciwork
pushd /tmp/ciwork

echo "Repository URL: ${CI_REPOSITORY_URL}"
echo "Branch: ${CI_COMMIT_REF_NAME}"
echo "Commit SHA: ${CI_COMMIT_SHA}"

_edo git clone ${CI_REPOSITORY_URL} ${REPO}
pushd ${REPO}
_edo git checkout ${CI_COMMIT_REF_NAME}

# Make sure that we always test all commits between the current HEAD and the official master.
# For the master itself we have to figure out which commits to build in a different way.
# Old documentation for CI_BUILD_BEFORE_SHA says that it specifies
# "The first commit that were included in push request".
# But this is actually not true. In some cases it is the last commit before the push,
# sometimes it is invalid and sometimes it is HEAD.
# Just build the last commit if it is HEAD and hope that it is the right thing to do.
PREV_REV="${CI_BUILD_BEFORE_SHA}"
echo "Previous SHA according to Gitlab: ${PREV_REV}"
if [[ ${PREV_REV} == "HEAD" ]]; then
    PREV_REV="${CI_COMMIT_SHA}^"
fi
if [[ ${CI_COMMIT_REF_NAME} != "master" ]] || [[ ${CI_PROJECT_NAMESPACE} != ${REPO_NAMESPACE} ]]; then
    _edo git remote add official https://gitlab.exherbo.org/${REPO_NAMESPACE}/${REPO}.git
    _edo git fetch official
    PREV_REV=remotes/official/master
fi
# Cut the changes in the master branch that are not part of this branch.
# We are not interested in them.
PREV_REV=$(git merge-base ${CI_COMMIT_SHA} ${PREV_REV})
echo "Previous SHA that will be used: ${PREV_REV}"

sed -e "/^sync/ s|$| local: git+file:///tmp/ciwork/${REPO}|" -i /etc/paludis/repositories/${PALUDIS_REPO}.conf
_edo cave sync ${PALUDIS_REPO} -s local -r ${CI_COMMIT_SHA}

declare -a PKGS UNTESTED_CHANGES
changes=$(git diff-tree --no-commit-id --diff-filter=d --name-only -r ${PREV_REV} ${CI_COMMIT_SHA})
while read line; do
    c='*' p='*' exlib= pnv=

    if [[ "${line}" =~ ^packages/([^/]+)/([^/]+)/([^/]+)\.exlib$ ]] ; then
        c=${BASH_REMATCH[1]}
        [[ ${BASH_REMATCH[2]} != exlibs ]] && p=${BASH_REMATCH[2]}
        exlib=${BASH_REMATCH[3]}
    elif [[ "${line}" =~ ^exlibs/([^/]+)\.exlib$ ]] ; then
        exlib=${BASH_REMATCH[1]}
    fi

    if [[ -n ${exlib} ]]; then
        exlib_pkgs=()
        # Our exlibs can only be used in repos that have us as a master.
        for r in ${PALUDIS_REPO} $(cave print-ids -m "repository/*[.dependencies<repository/${PALUDIS_REPO}]" -f '%p\n'); do
            # Need to build new-style spec for use in config files below
            exlib_pkgs+=( $(cave print-ids -s install -m "${c}/${p}::${r}[.INHERITED<${exlib}]" -f '%c/%p%:%s::%r[=%V]\n') )
        done

        if [[ ${#exlib_pkgs[@]} -gt 50 ]]; then
            echo "exlib ${exlib} has many consumers, picking 50 to test at random"
            PKGS+=( $(shuf -n50 -e "${exlib_pkgs[@]}" ) )
        elif [[ ${#exlib_pkgs[@]} -gt 0 ]]; then
            PKGS+=( "${exlib_pkgs[@]}" )
        else
            echo "Couldn't find any exheres that require ${exlib}.exlib"
            UNTESTED_CHANGES+=( "${line}" )
        fi
    elif [[ "${line}" =~ ^packages/[^/]+/[^/]+/([^/]+)\.exheres-0$ ]] ; then
        pnv=${BASH_REMATCH[1]}
        # Need to build new-style spec for use in config files below
        exheres_pkgs=( $(cave print-ids -s install -m "=${pnv}::${PALUDIS_REPO}" -f '%c/%p%:%s::%r[=%V]\n') )

        if [[ ${#exheres_pkgs[@]} -gt 0 ]]; then
            PKGS+=( "${exheres_pkgs[@]}" )
        else
            echo "Couldn't find any exheres that match =${pnv}::${PALUDIS_REPO}"
            UNTESTED_CHANGES+=( "${line}" )
        fi
    else
        UNTESTED_CHANGES+=( "${line}" )
    fi
done <<< "${changes}"

# Get rid of duplicates.
IFS=$'\n' PKGS=( $(sort <<<"${PKGS[*]}" | uniq) )
unset IFS

if [[ ${#PKGS[@]} -gt 0 ]]; then
    echo "These are the packages that I will build:"
    echo ${PKGS[@]}
else
    echo "No package to build. No exlib recognised. Don't know what to do. Exiting."
    exit 0
fi

popd
popd

# Set up pbins
pushd /pbins
mkdir -p {dist,pbin{,/metadata,/packages,/profiles}}
echo 'pbin' > pbin/profiles/repo_name
echo 'masters = arbor' > pbin/metadata/layout.conf
touch pbin/profiles/options.conf
touch pbin/metadata/categories.conf
chown -R paludisbuild:paludisbuild .
chmod -R g+w .
popd

cat <<EOF > /etc/paludis/repositories/pbin.conf
format = e
location = /pbins/pbin
distdir = /pbins/dist
binary_distdir = /pbins/dist
binary_destination = true
binary_keywords_filter = amd64 ~amd64
tool_prefix = x86_64-pc-linux-gnu-
importance = -10
EOF

# Only build pbins if this is the master branch in one of our repositories
declare -a VIA_BIN
if [[ ${CI_COMMIT_REF_NAME} == "master" ]]; then
    if [[ ${CI_PROJECT_NAMESPACE} == "exherbo" || \
         ${CI_PROJECT_NAMESPACE} == "exherbo-unofficial" || \
         ${CI_PROJECT_NAMESPACE} == "exherbo-devs" ]]; then
        VIA_BIN=("--via-binary" "*/*" "--one-binary-per-slot")
    fi
fi

# Only enable tests for the target
mkdir -p /etc/paludis/{options.conf.d,package_mask.conf.d}
echo '*/* build_options: -recommended_tests' > /etc/paludis/options.conf.d/no_tests.conf

# We are explicitely handling errors from here on
set +e
declare -a FAILED_PKGS SUCCEEDED_PKGS MSCAN_RESULTS
for PKG in "${PKGS[@]}"; do
    echo "**************************************************************"
    echo "Building package ${PKG}"
    echo "**************************************************************"

    # ensure that tests run for the target and that it isn't installed from an existing pbin
    # interestingly, this still allows --via-binary to work for the target, which is exactly what we want!
    # NOTE(moben): shouldn't technically be needed as we include the repo in the ${PKG} spec, but better be safe
    echo "${PKG} build_options: recommended_tests" > /etc/paludis/options.conf.d/target_tests.conf
    echo "${PKG/::*\[/::pbin\[}" > /etc/paludis/package_mask.conf.d/target_pbin.conf

    # Install all repositories for dependencies in ::unavailable.
    # This is a bit hacky but works for now.
    # --hide ::pbin to ensure that we install the origin repos in case they contain users/groups we need
    declare -a NEW_REPOS
    while
        NEW_REPOS=$(cave resolve --hide '*/*::pbin' ${PKG} | grep -E '::unavailable(-unofficial)? \(in ::[A-Za-z0-9+_.-]*\)' -o | sed -e 's#::unavailable.* (in ::\([^)]\+\))#repository/\1#g')
        [[ ${NEW_REPOS[@]} ]]
    do
        _edo cave resolve -Ks ${NEW_REPOS} -x
    done

    # Handle confirmations automatically
    # NOTE(moben): --execute-resolution-program true should also suppress the "unread news" hook but doesn't in the container?
    TMPFILE=$(mktemp -uq)
    cave resolve "${VIA_BIN[@]}" ${PKG} --execute-resolution-program true \
        --display-resolution-program "cave print-resolution-required-confirmations" > ${TMPFILE}
    ARGS=$(handle_confirmations < ${TMPFILE})
    rc=$?
    if [[ $rc -gt 0 ]]; then
        echo "***** I FAILED! ***********************************************"
        cat ${TMPFILE}
        echo "*************** COMMITTING SUICIDE NOW! ***********************"
        FAILED_PKGS+=( ${PKG} )
        continue
    fi

    # Perform the build itself
    echo "ARGS: ${ARGS}"
    echo "**************************************************************"
    _edo cave resolve "${VIA_BIN[@]}" --promote-binaries if-same -zx ${PKG} ${ARGS}
    rc=$?
    if [[ ${rc} -gt 0 ]]; then
        echo "Build failed!"
        FAILED_PKGS+=( ${PKG} )
        continue
    else
        SUCCEEDED_PKGS+=( ${PKG} )
    fi

    # Check dependencies
    MSCAN_RESULTS+=( "$(
        echo "**************************************************************"
        echo "Dependencies I believe to have found for ${PKG} (excluding system):"
        /usr/local/bin/mscan2.rb -i system --hide-libs unused ${PKG/::*\[/\[} 2>&1
    )" )
done

# Get rid of now unused pbin tarballs
# (also any distfiles we fetched but there shouldn't be a lot of those if pbins are working correctly)
# only delete files older than 1h to avoid races between one job creating a pbin tarball
# and another deleting it before the pbin exheres could be written
if [[ -n "${VIA_BIN[@]}" ]]; then
  _edo cave print-unused-distfiles -i pbin | xargs -I{files} find {files} -not -mmin -60 -exec rm {} +
fi

if [[ ${#MSCAN_RESULTS[@]} -gt 0 ]]; then
    echo "**************************************************************"
    echo "mscan2.rb results: ${MSCAN_RESULTS[@]/#/$'\n'}"
    echo "**************************************************************"
fi
if [[ ${#SUCCEEDED_PKGS[@]} -gt 0 ]]; then
    echo "Builds that succeeded: ${SUCCEEDED_PKGS[@]/#/$'\n'    }"
    echo "**************************************************************"
fi
if [[ ${#FAILED_PKGS[@]} -gt 0 ]]; then
    echo "Builds that failed: ${FAILED_PKGS[@]/#/$'\n'    }"
    echo "**************************************************************"
fi
if [[ ${#UNTESTED_CHANGES[@]} -gt 0 ]]; then
    echo "I was unable to find packages to test changes to these files: ${UNTESTED_CHANGES[@]/#/$'\n'    }"
    echo "**************************************************************"
fi

[[ ${#FAILED_PKGS[@]} -gt 0 ]] && exit 1 || exit 0

